from django.conf.urls import patterns, url, include

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()


urlpatterns = patterns('mashup.views',
  (r'^$', 'index'),
  (r'api/census/income/region/(?P<rid>[A-Z]+)/$', 'get_json_census_income_region'),
  (r'api/census/income/postcode/(?P<pid>[0-9]+)/$', 'get_json_census_income_postcode'),
  (r'api/census/income/electorate/(?P<eid>.+)/$', 'get_json_census_income_electorate'),
  (r'api/region/list/$', 'get_region_list'),
  (r'api/postcode/list/$', 'get_postcode_list'),
  (r'api/suburb/list/$', 'get_suburb_list'),
  (r'api/electorate/list/$', 'get_electorate_list'),
  (r'api/tax/income/region/(?P<rid>[A-Z]+)/$', 'get_json_tax_income_region'),
  (r'api/tax/income/postcode/(?P<pid>[0-9]+)/$', 'get_json_tax_income_postcode'),
  (r'api/gdp/$', 'get_json_gdp'),
)

