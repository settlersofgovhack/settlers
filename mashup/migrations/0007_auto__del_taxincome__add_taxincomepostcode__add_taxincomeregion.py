# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting model 'TaxIncome'
        db.delete_table(u'mashup_taxincome')

        # Adding model 'TaxIncomePostcode'
        db.create_table(u'mashup_taxincomepostcode', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('postcode', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mashup.Postcode'])),
            ('individuals', self.gf('django.db.models.fields.IntegerField')()),
            ('taxable_income', self.gf('django.db.models.fields.IntegerField')()),
            ('gross_tax', self.gf('django.db.models.fields.IntegerField')()),
            ('net_tax', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal(u'mashup', ['TaxIncomePostcode'])

        # Adding model 'TaxIncomeRegion'
        db.create_table(u'mashup_taxincomeregion', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('region', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mashup.Region'])),
            ('individuals', self.gf('django.db.models.fields.IntegerField')()),
            ('taxable_income', self.gf('django.db.models.fields.IntegerField')()),
            ('gross_tax', self.gf('django.db.models.fields.IntegerField')()),
            ('net_tax', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal(u'mashup', ['TaxIncomeRegion'])


    def backwards(self, orm):
        # Adding model 'TaxIncome'
        db.create_table(u'mashup_taxincome', (
            ('gross_tax', self.gf('django.db.models.fields.IntegerField')()),
            ('individuals', self.gf('django.db.models.fields.IntegerField')()),
            ('taxable_income', self.gf('django.db.models.fields.IntegerField')()),
            ('net_tax', self.gf('django.db.models.fields.IntegerField')()),
            ('postcode', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mashup.Postcode'])),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal(u'mashup', ['TaxIncome'])

        # Deleting model 'TaxIncomePostcode'
        db.delete_table(u'mashup_taxincomepostcode')

        # Deleting model 'TaxIncomeRegion'
        db.delete_table(u'mashup_taxincomeregion')


    models = {
        u'mashup.censuselectorateincome': {
            'Meta': {'object_name': 'CensusElectorateIncome'},
            'band': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mashup.IncomeBand']"}),
            'electorate': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mashup.Electorate']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'value': ('django.db.models.fields.IntegerField', [], {})
        },
        u'mashup.censuspostcodeincome': {
            'Meta': {'object_name': 'CensusPostcodeIncome'},
            'band': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mashup.IncomeBand']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'postcode': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mashup.Postcode']"}),
            'value': ('django.db.models.fields.IntegerField', [], {})
        },
        u'mashup.censusregionincome': {
            'Meta': {'object_name': 'CensusRegionIncome'},
            'band': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mashup.IncomeBand']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mashup.Region']"}),
            'value': ('django.db.models.fields.IntegerField', [], {})
        },
        u'mashup.electorate': {
            'Meta': {'object_name': 'Electorate'},
            'code': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '16'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        u'mashup.incomeband': {
            'Meta': {'object_name': 'IncomeBand'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'label': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '32'}),
            'max': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'min': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '250'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        u'mashup.postcode': {
            'Meta': {'object_name': 'Postcode'},
            'code': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '5'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'mashup.region': {
            'Meta': {'object_name': 'Region'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'short': ('django.db.models.fields.CharField', [], {'max_length': '6'})
        },
        u'mashup.suburb': {
            'Meta': {'object_name': 'Suburb'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '200'}),
            'postcode': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mashup.Postcode']"})
        },
        u'mashup.taxincomepostcode': {
            'Meta': {'object_name': 'TaxIncomePostcode'},
            'gross_tax': ('django.db.models.fields.IntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'individuals': ('django.db.models.fields.IntegerField', [], {}),
            'net_tax': ('django.db.models.fields.IntegerField', [], {}),
            'postcode': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mashup.Postcode']"}),
            'taxable_income': ('django.db.models.fields.IntegerField', [], {})
        },
        u'mashup.taxincomeregion': {
            'Meta': {'object_name': 'TaxIncomeRegion'},
            'gross_tax': ('django.db.models.fields.IntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'individuals': ('django.db.models.fields.IntegerField', [], {}),
            'net_tax': ('django.db.models.fields.IntegerField', [], {}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mashup.Region']"}),
            'taxable_income': ('django.db.models.fields.IntegerField', [], {})
        }
    }

    complete_apps = ['mashup']