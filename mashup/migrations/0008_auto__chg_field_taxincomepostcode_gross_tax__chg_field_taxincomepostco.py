# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'TaxIncomePostcode.gross_tax'
        db.alter_column(u'mashup_taxincomepostcode', 'gross_tax', self.gf('django.db.models.fields.BigIntegerField')())

        # Changing field 'TaxIncomePostcode.taxable_income'
        db.alter_column(u'mashup_taxincomepostcode', 'taxable_income', self.gf('django.db.models.fields.BigIntegerField')())

        # Changing field 'TaxIncomePostcode.individuals'
        db.alter_column(u'mashup_taxincomepostcode', 'individuals', self.gf('django.db.models.fields.BigIntegerField')())

        # Changing field 'TaxIncomePostcode.net_tax'
        db.alter_column(u'mashup_taxincomepostcode', 'net_tax', self.gf('django.db.models.fields.BigIntegerField')())

        # Changing field 'TaxIncomeRegion.gross_tax'
        db.alter_column(u'mashup_taxincomeregion', 'gross_tax', self.gf('django.db.models.fields.BigIntegerField')())

        # Changing field 'TaxIncomeRegion.taxable_income'
        db.alter_column(u'mashup_taxincomeregion', 'taxable_income', self.gf('django.db.models.fields.BigIntegerField')())

        # Changing field 'TaxIncomeRegion.individuals'
        db.alter_column(u'mashup_taxincomeregion', 'individuals', self.gf('django.db.models.fields.BigIntegerField')())

        # Changing field 'TaxIncomeRegion.net_tax'
        db.alter_column(u'mashup_taxincomeregion', 'net_tax', self.gf('django.db.models.fields.BigIntegerField')())

    def backwards(self, orm):

        # Changing field 'TaxIncomePostcode.gross_tax'
        db.alter_column(u'mashup_taxincomepostcode', 'gross_tax', self.gf('django.db.models.fields.IntegerField')())

        # Changing field 'TaxIncomePostcode.taxable_income'
        db.alter_column(u'mashup_taxincomepostcode', 'taxable_income', self.gf('django.db.models.fields.IntegerField')())

        # Changing field 'TaxIncomePostcode.individuals'
        db.alter_column(u'mashup_taxincomepostcode', 'individuals', self.gf('django.db.models.fields.IntegerField')())

        # Changing field 'TaxIncomePostcode.net_tax'
        db.alter_column(u'mashup_taxincomepostcode', 'net_tax', self.gf('django.db.models.fields.IntegerField')())

        # Changing field 'TaxIncomeRegion.gross_tax'
        db.alter_column(u'mashup_taxincomeregion', 'gross_tax', self.gf('django.db.models.fields.IntegerField')())

        # Changing field 'TaxIncomeRegion.taxable_income'
        db.alter_column(u'mashup_taxincomeregion', 'taxable_income', self.gf('django.db.models.fields.IntegerField')())

        # Changing field 'TaxIncomeRegion.individuals'
        db.alter_column(u'mashup_taxincomeregion', 'individuals', self.gf('django.db.models.fields.IntegerField')())

        # Changing field 'TaxIncomeRegion.net_tax'
        db.alter_column(u'mashup_taxincomeregion', 'net_tax', self.gf('django.db.models.fields.IntegerField')())

    models = {
        u'mashup.censuselectorateincome': {
            'Meta': {'object_name': 'CensusElectorateIncome'},
            'band': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mashup.IncomeBand']"}),
            'electorate': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mashup.Electorate']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'value': ('django.db.models.fields.IntegerField', [], {})
        },
        u'mashup.censuspostcodeincome': {
            'Meta': {'object_name': 'CensusPostcodeIncome'},
            'band': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mashup.IncomeBand']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'postcode': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mashup.Postcode']"}),
            'value': ('django.db.models.fields.IntegerField', [], {})
        },
        u'mashup.censusregionincome': {
            'Meta': {'object_name': 'CensusRegionIncome'},
            'band': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mashup.IncomeBand']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mashup.Region']"}),
            'value': ('django.db.models.fields.IntegerField', [], {})
        },
        u'mashup.electorate': {
            'Meta': {'object_name': 'Electorate'},
            'code': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '16'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        u'mashup.incomeband': {
            'Meta': {'object_name': 'IncomeBand'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'label': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '32'}),
            'max': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'min': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '250'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        u'mashup.postcode': {
            'Meta': {'object_name': 'Postcode'},
            'code': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '5'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'mashup.region': {
            'Meta': {'object_name': 'Region'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'short': ('django.db.models.fields.CharField', [], {'max_length': '6'})
        },
        u'mashup.suburb': {
            'Meta': {'object_name': 'Suburb'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '200'}),
            'postcode': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mashup.Postcode']"})
        },
        u'mashup.taxincomepostcode': {
            'Meta': {'object_name': 'TaxIncomePostcode'},
            'gross_tax': ('django.db.models.fields.BigIntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'individuals': ('django.db.models.fields.BigIntegerField', [], {}),
            'net_tax': ('django.db.models.fields.BigIntegerField', [], {}),
            'postcode': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mashup.Postcode']"}),
            'taxable_income': ('django.db.models.fields.BigIntegerField', [], {})
        },
        u'mashup.taxincomeregion': {
            'Meta': {'object_name': 'TaxIncomeRegion'},
            'gross_tax': ('django.db.models.fields.BigIntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'individuals': ('django.db.models.fields.BigIntegerField', [], {}),
            'net_tax': ('django.db.models.fields.BigIntegerField', [], {}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mashup.Region']"}),
            'taxable_income': ('django.db.models.fields.BigIntegerField', [], {})
        }
    }

    complete_apps = ['mashup']