# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Postcode'
        db.create_table(u'mashup_postcode', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('code', self.gf('django.db.models.fields.CharField')(unique=True, max_length=5)),
        ))
        db.send_create_signal(u'mashup', ['Postcode'])

        # Adding model 'Suburb'
        db.create_table(u'mashup_suburb', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('postcode', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mashup.Postcode'])),
            ('name', self.gf('django.db.models.fields.CharField')(default='', max_length=200)),
        ))
        db.send_create_signal(u'mashup', ['Suburb'])

        # Adding model 'Region'
        db.create_table(u'mashup_region', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('short', self.gf('django.db.models.fields.CharField')(max_length=6)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=32)),
        ))
        db.send_create_signal(u'mashup', ['Region'])

        # Adding model 'IncomeBand'
        db.create_table(u'mashup_incomeband', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(default='', max_length=250)),
            ('min', self.gf('django.db.models.fields.IntegerField')(blank=True)),
            ('max', self.gf('django.db.models.fields.IntegerField')(blank=True)),
        ))
        db.send_create_signal(u'mashup', ['IncomeBand'])

        # Adding model 'CensusRegionIncome'
        db.create_table(u'mashup_censusregionincome', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('region', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mashup.Region'])),
            ('band', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mashup.IncomeBand'])),
            ('value', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal(u'mashup', ['CensusRegionIncome'])

        # Adding model 'CensusPostcodeIncome'
        db.create_table(u'mashup_censuspostcodeincome', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('postcode', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mashup.Postcode'])),
            ('band', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mashup.IncomeBand'])),
            ('value', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal(u'mashup', ['CensusPostcodeIncome'])


    def backwards(self, orm):
        # Deleting model 'Postcode'
        db.delete_table(u'mashup_postcode')

        # Deleting model 'Suburb'
        db.delete_table(u'mashup_suburb')

        # Deleting model 'Region'
        db.delete_table(u'mashup_region')

        # Deleting model 'IncomeBand'
        db.delete_table(u'mashup_incomeband')

        # Deleting model 'CensusRegionIncome'
        db.delete_table(u'mashup_censusregionincome')

        # Deleting model 'CensusPostcodeIncome'
        db.delete_table(u'mashup_censuspostcodeincome')


    models = {
        u'mashup.censuspostcodeincome': {
            'Meta': {'object_name': 'CensusPostcodeIncome'},
            'band': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mashup.IncomeBand']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'postcode': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mashup.Postcode']"}),
            'value': ('django.db.models.fields.IntegerField', [], {})
        },
        u'mashup.censusregionincome': {
            'Meta': {'object_name': 'CensusRegionIncome'},
            'band': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mashup.IncomeBand']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mashup.Region']"}),
            'value': ('django.db.models.fields.IntegerField', [], {})
        },
        u'mashup.incomeband': {
            'Meta': {'object_name': 'IncomeBand'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max': ('django.db.models.fields.IntegerField', [], {'blank': 'True'}),
            'min': ('django.db.models.fields.IntegerField', [], {'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '250'})
        },
        u'mashup.postcode': {
            'Meta': {'object_name': 'Postcode'},
            'code': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '5'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'mashup.region': {
            'Meta': {'object_name': 'Region'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'short': ('django.db.models.fields.CharField', [], {'max_length': '6'})
        },
        u'mashup.suburb': {
            'Meta': {'object_name': 'Suburb'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '200'}),
            'postcode': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['mashup.Postcode']"})
        }
    }

    complete_apps = ['mashup']