from django.contrib import admin
from mashup.models import *

# Register your models here.
admin.site.register(Postcode)
admin.site.register(Suburb)
admin.site.register(Region)
admin.site.register(Electorate)
admin.site.register(IncomeBand)
admin.site.register(CensusRegionIncome)
admin.site.register(CensusPostcodeIncome)
admin.site.register(CensusElectorateIncome)
admin.site.register(TaxIncomeRegion)
admin.site.register(TaxIncomePostcode)
admin.site.register(AusGDP)