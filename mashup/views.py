from django.shortcuts import render
from django.http import HttpResponse, HttpRequest, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render_to_response
from django.template import RequestContext, loader
import json, datetime, copy
from mashup.models import *

def index(request):
  context_dict = { }
  e = Electorate.objects.all()
  context_dict = { 'electorates': e }  
  return render_to_response('index.html', context_dict, context_instance=RequestContext(request))

def get_json_census_income_region(request, rid=None):
  out = {}
  if rid is None:
    pass
    #get all region data
  else:
    out = CensusRegionIncome.get_income(rid)
  return HttpResponse(json.dumps(out), content_type="application/json")

def get_json_census_income_postcode(request, pid=None):
  out = {}
  if pid is None:
    pass
    #get all postcodes #TODO
  else:
    out = CensusPostcodeIncome.get_income(str(pid))
  return HttpResponse(json.dumps(out), content_type="application/json")

def get_json_census_income_electorate(request, eid=None):
  out = {}
  if eid is None:
    pass
    #get all postcodes #TODO
  else:
    out = CensusElectorateIncome.get_income(eid)
  return HttpResponse(json.dumps(out), content_type="application/json")

def get_region_list(request):
  out = Region.get_regions()
  return HttpResponse(json.dumps(out), content_type="application/json")
  
def get_postcode_list(request):
  out = Postcode.get_postcodes()
  return HttpResponse(json.dumps(out), content_type="application/json")
  
def get_suburb_list(request):
  out = Suburb.get_postcodes()
  return HttpResponse(json.dumps(out), content_type="application/json")
  
def get_electorate_list(request):
  out = Electorate.get_electorates()
  return HttpResponse(json.dumps(out), content_type="application/json")

def get_json_tax_income_region(request, rid=None):
  out = {}
  if rid is None:
    pass
    #get all regions # TODO
  else:
    out = TaxIncomeRegion.get_income(rid)
  return HttpResponse(json.dumps(out), content_type="application/json")

def get_json_tax_income_postcode(request, pid=None):
  out = {}
  if pid is None:
    pass
    #get all postcodes # TODO
  else:
    out = TaxIncomePostcode.get_income(pid)
  return HttpResponse(json.dumps(out), content_type="application/json")

def get_json_gdp(request):
  out = AusGDP.get_gdp();
  return HttpResponse(json.dumps(out), content_type="application/json")
  