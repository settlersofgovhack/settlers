from django.db import models

# Create your models here.

class Postcode(models.Model):
  class Meta:
    verbose_name = "Postcode"
    verbose_name_plural = "Postcodes"
  
  code = models.CharField(max_length=5, unique=True)
  
  def __unicode__(self):
    return self.code
  
  @staticmethod
  def get_postcodes():
    r = Postcode.objects.all()
    out = { 'postcodes': [] }
    for x in r:
      out['postcodes'].append({'postcode': x.code})
    return out
    
class Suburb(models.Model):
  class Meta:
    verbose_name = "Suburb"
    verbose_name_plural = "Suburbs"
  
  postcode = models.ForeignKey(Postcode)
  name = models.CharField(max_length=200, default='')
  
  def __unicode__(self):
    return self.name
  
  @staticmethod
  def get_suburbs():
    r = Suburb.objects.all()
    out = { 'suburbs': [] }
    for x in r:
      out['suburbs'].append({'postcode': x.postcode.code, 'suburb': x.name})
    return out

class Region(models.Model):
  class Meta:
    verbose_name = "Region"
    verbose_name_plural = "Regions"
  
  short = models.CharField(max_length=6)
  name = models.CharField(max_length=32)
  
  def __unicode__(self):
    return self.name

  @staticmethod
  def get_regions():
    r = Region.objects.all()
    out = { 'regions': [] }
    for x in r:
      if x.short != 'AUST':
        out['regions'].append({'short': x.short, 'name': x.name})
    return out        

class Electorate(models.Model):
  class Meta:
    verbose_name = "Electorate"
    verbose_name_plural = "Electorates"
  
  code = models.CharField(max_length=16, unique=True)
  name = models.CharField(max_length=128)
  
  def __unicode__(self):
    return self.name
  
  @staticmethod
  def get_electorates():
    r = Electorate.objects.all()
    out = { 'electorates': [] }
    for x in r:
      out['electorates'].append({'code': x.code, 'name': x.name})
    return out

class IncomeBand(models.Model):
  class Meta:
    verbose_name = "Income Band"
    verbose_name_plural = "Income Bands"
  name = models.CharField(max_length=250, verbose_name="Income Band", default='')
  order = models.IntegerField(default=0)
  label = models.CharField(max_length=32, verbose_name="Chart Label", default='')
  min = models.IntegerField(null=True, blank=True)
  max = models.IntegerField(null=True, blank=True)

  def __unicode__(self):
    return self.name + ' - ' + str(self.order)

class CensusRegionIncome(models.Model):
  class Meta:
    verbose_name = "Census Region Income By Band"
    verbose_name_plural = "Census Region Income By Bands"
  
  region = models.ForeignKey(Region)
  band = models.ForeignKey(IncomeBand)
  value = models.IntegerField()

  def __unicode__(self):
    return self.region.short + ' - ' + self.band.name + ': ' + str(self.value)

  @staticmethod
  def get_income(region):
    try:
      r = Region.objects.get(short=region)
    except Region.DoesNotExist:
      return None
    out = {}
    i = CensusRegionIncome.objects.filter(region=r)
    out["region"] = r.name
    out["income"] = []
    for ib in i:
      out["income"].append({'band': ib.band.label, 'bandorder': ib.band.order, 'value': ib.value})
    return out

class CensusPostcodeIncome(models.Model):
  class Meta:
    verbose_name = "Census Postcode By Band"
    verbose_name_plural = "Census Postcode By Bands"
  
  postcode = models.ForeignKey(Postcode)
  band = models.ForeignKey(IncomeBand)
  value = models.IntegerField()

  def __unicode__(self):
    return self.postcode.code + ' - ' + self.band.name + ': ' + str(self.value)
    
  @staticmethod
  def get_income(postcode):
    try:
      p = Postcode.objects.get(code=postcode)
    except Postcode.DoesNotExist:
      return None
    out = {}
    i = CensusPostcodeIncome.objects.filter(postcode=p)
    out["region"] = p.code
    out["income"] = []
    for ib in i:
      out["income"].append({'band': ib.band.label, 'bandorder': ib.band.order, 'value': ib.value})
    return out

class CensusElectorateIncome(models.Model):
  class Meta:
    verbose_name = "Census Electorate By Band"
    verbose_name_plural = "Census Electorate By Bands"
  
  electorate = models.ForeignKey(Electorate)
  band = models.ForeignKey(IncomeBand)
  value = models.IntegerField()

  def __unicode__(self):
    return self.electorate.name + ' - ' + self.band.name + ': ' + str(self.value)
    
  @staticmethod
  def get_income(electorate):
    try:
      e = Electorate.objects.get(code=electorate)
    except Postcode.DoesNotExist:
      return None
    out = {}
    i = CensusElectorateIncome.objects.filter(electorate=e)
    out["region"] = e.name
    out["income"] = []
    for ib in i:
      out["income"].append({'band': ib.band.label, 'bandorder': ib.band.order, 'value': ib.value})
    return out

class TaxIncomePostcode(models.Model):
  class Meta:
    verbose_name = "Tax Income by Postcode"
    verbose_name_plural = "Tax Income by Postcodes"
  
  postcode = models.ForeignKey(Postcode)
  individuals = models.BigIntegerField()
  taxable_income = models.BigIntegerField()
  gross_tax = models.BigIntegerField()
  net_tax = models.BigIntegerField()

  def __unicode__(self):
    return self.postcode.code + ': ' + str(self.individuals) + ' - ' + str(self.taxable_income)

  @staticmethod
  def get_income(pcode):
    r = Postcode.objects.get(code=pcode)
    out = {}
    t = TaxIncomeRegion(postcode=r)
    out['taxable_income'] = t.taxable_income
    out['individuals'] = t.individuals
    out['gross_tax'] = t.gross_tax
    out['net_tax'] = t.net_tax
    return out

class TaxIncomeRegion(models.Model):
  class Meta:
    verbose_name = "Tax Income by Region"
    verbose_name_plural = "Tax Income by Regions"
  
  region = models.ForeignKey(Region)
  individuals = models.BigIntegerField()
  taxable_income = models.BigIntegerField()
  gross_tax = models.BigIntegerField()
  net_tax = models.BigIntegerField()

  def __unicode__(self):
    return self.region.short + ': ' + str(self.individuals) + ' - ' + str(self.taxable_income)

  @staticmethod
  def get_income(region):
    r = Region.objects.get(short=region)
    out = {}
    t = TaxIncomeRegion.objects.get(region=r)
    out['taxable_income'] = t.taxable_income
    out['individuals'] = t.individuals
    out['gross_tax'] = t.gross_tax
    out['net_tax'] = t.net_tax
    return out

class AusGDP(models.Model):
  class Meta:
    verbose_name = "Australia GDP"
    verbose_name_plural = "Australia GDP"
  
  region = models.ForeignKey(Region)
  gdp = models.BigIntegerField()
  
  def __unicode__(self):
    return self.region.short + ': ' + str(self.gdp)
  
  @staticmethod
  def get_gdp():
    out = { 'gdp': {}}
    g = AusGDP.objects.all()
    for x in g:
      out['gdp'][x.region.short] = x.gdp
    return out
