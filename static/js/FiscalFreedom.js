
function CalcFiscalFreedom(gdp,net_tax,taxable_income, num_people)
{
	var average_taxable_income = 0;
	
	var factor1=0;
	var factor2=0;
	var factor3=0;
	
	average_taxable_income = taxable_income / num_people;
	
	factor1 = calcTopMarginalTaxRate(average_taxable_income);
	factor2 = calcCorpMarginalTax();
	factor3 = calcTaxPercentage(net_tax,gdp);
	

	return (factor1+factor2+factor3)/3;
}

function calcTopMarginalTaxRate(average_taxable_income)
{
	mt = 0;
	
	if (average_taxable_income < 6000)
	{
		mt = 0.0;
		return 100- 0.03 * Math.pow(mt,2);
	}
	
	if (average_taxable_income < 37000)
	{
		mt= 15;
		return 100- 0.03 * Math.pow(mt,2);
	}
	
	if (average_taxable_income < 80000)
	{
		mt= 30;
		return 100- 0.03 * Math.pow(mt,2);
	}
	
	if (average_taxable_income < 180000)
	{
		mt= 37;
		return 100- 0.03 * Math.pow(mt,2);
	}
	
	if (average_taxable_income > 180000)
	{
		mt= 45;
		return 100- 0.03 * Math.pow(mt,2);
	}
	
	
	return 100- 0.03 * Math.pow(mt,2);
}

function calcCorpMarginalTax()
{
	return 100 - 0.03 * Math.pow(30,2);
}

function calcTaxPercentage(net_tax,gdp)
{
	return 100 - 0.03 * Math.pow((net_tax/gdp)*100,2);
}

