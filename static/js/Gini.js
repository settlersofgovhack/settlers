// Gini.js - Calculates Gini co-efficient
// Author: Andrew Kruup
// For Govhack 2014


function calcTriangle(x1,y1,x2,y2)
{
	var width = 0;
	var height = 0;
	
	var minx = Math.min(x1,x2);
	var miny = Math.min(y1,y2);
	var maxx = Math.max(x1,x2);
	var maxy = Math.max(y1,y2);
	
	width = maxx-minx;
	height = maxy - miny;
	
	return (width*height)/2;
}

function calcRect(x1,y1,x2,y2)
{
	var width = 0;
	var height = 0;
	
	var minx = Math.min(x1,x2);
	var miny = Math.min(y1,y2);
	var maxx = Math.max(x1,x2);
	var maxy = Math.max(y1,y2);
	
	width = maxx - minx;
	height = miny;
	
	return width*height;
}

function CalcGiniForRegion(population)
{
var total_income =0 ;
 var total_population = 0; // stores total population
 var income_brackets = [0,99.5,250,350,500,700,900,1125,1375,1750,3000]; //income brackets - assumed upper income maximum of 4000
 
 var percentage_population = []; 
 var percentage_income = [];
 
 var cumul_population = [];
 var cumul_income = [];
 
 var b=0;
 var equalityArea=(1*1)/2;
 var a=0;

 
	for (i = 0; i < population.length; i++) // calculate total_population size in order to calculate percentages
	{
		total_population = total_population + population[i];
	}
	
	
	for (i = 0; i < population.length; i++) // calculate total_population size in order to calculate percentages
	{
		percentage_population[i] = population[i] / total_population;
	}
	
	for (i=0; i< population.length; i++)
	{
		total_income = total_income + population[i] * income_brackets[i];
	}
	
	
	for (i=0; i<population.length; i++)
	{
		percentage_income[i] = (population[i] * income_brackets[i]) / total_income;
	}
	
	
	cumul_income[0] = percentage_income[0];

	cumul_population[0] = percentage_population[0];	
	
	for (i=1; i<population.length; i++)
	{
		cumul_income[i] = cumul_income[i-1] + percentage_income[i];
		
		cumul_population[i] = cumul_population[i-1] + percentage_population[i];
	}
	
	for (i =1; i < cumul_population.length; i++)
	{
		b = b + calcRect(cumul_population[i],cumul_income[i], cumul_population[i-1], cumul_income[i-1]) + calcTriangle(cumul_population[i],cumul_income[i], cumul_population[i-1], cumul_income[i-1]);
	}
	
	a=equalityArea-b;
	
	return {
		'coeff': (a/(a+b)).toFixed(3),
		'cumul_income': cumul_income,
		'cumul_population': cumul_population,
	};
	
}
